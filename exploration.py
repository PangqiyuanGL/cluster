import pandas as pd
import numpy as np
import copy
import sys
import matplotlib.pyplot as plt

def main():
    np.random.seed(0)
    N = 20000
    digits = pd.read_csv('digits-raw.csv')
    digits = digits.to_numpy()
    ind = np.random.randint(0, N, size=1)
    number = digits[ind[0]][2:]
    print('selected number:', digits[ind[0]][1])
    matrix = np.reshape(number, [28,28])
    plt.imshow(matrix, cmap='gray')
    plt.savefig('num.png')
    plt.show()
    
    digits_emb = pd.read_csv('digits-embedding.csv').to_numpy()
    size = 1000
    indices = np.random.randint(0, N-1, size=size)
    x = np.zeros(size)
    y = np.zeros(size)
    c = np.zeros(size)
    for i in range(size):
        #print(indices[i])
        x[i] = digits_emb[indices[i]][2]
        y[i] = digits_emb[indices[i]][3]
        c[i] = digits_emb[indices[i]][1]
    plt.scatter(x, y, c=c)
    plt.savefig('dist.png')
    plt.show()
    
if __name__ == '__main__':
    main()
    
