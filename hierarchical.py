import pandas as pd
import numpy as np
import copy
import sys
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from kmeans import KMean
from scipy import cluster

def classify(data):
    ind = dict()
    for i in range(10):
        ind[i] = []
    for i in range(len(data)):
        ind[int(data[i, 1])].append(i)
    return ind

def cut(c, k):
    ind = dict()
    for i in k:
        ind[i] = []
    for i in range(len(c)):
        #print(c[i], k)
        ind[c[i]].append(i)
    return ind

def prob(ind):
    p = dict()
    count = 0.0
    for key in list(ind.keys()):
        p[key] = 0.0
        count += len(ind[key])
    for key in list(ind.keys()):
        p[key] = len(ind[key])/count
    return p
    
def matrix_p(c, g, kc, kg):
    indc = cut(c, kc)
    indg = cut(g, kg)
    n = len(c)
    p = np.zeros([len(kc), len(kg)])
    for i in range(len(kc)):
        for j in range(len(kg)):
            its = list(set(indc[kc[i]]).intersection(set(indg[kg[j]])))
            p[i, j] = len(its)*1.0/n
    return p
    
def NMI(c, g, kc, kg):
    p = matrix_p(c, g, kc, kg)
    indc = cut(c, kc)
    pc = prob(indc)
    indg = cut(g, kg)
    pg = prob(indg)
    
    I = 0.0
    for i in range(len(kc)):
        for j in range(len(kg)):
            if p[i, j] > 0:
                I += p[i, j]*np.log(p[i,j]/pc[kc[i]]/pg[kg[j]])
    
    hc = 0.0
    for key in list(pc.keys()):
        hc -= pc[key]*np.log(pc[key])
    hg = 0.0
    for key in list(pg.keys()):
        hg -= pg[key]*np.log(pg[key])
            
    nmi = I/(hc+hg)
    return nmi

def mean(vecs):
    m = vecs[0]
    for i in range(1, len(vecs)):
        m += vecs[i]/len(vecs)
    return m

def WCSSD(data, c, k):
    ind = cut(c, k)
    wcssd = 0.0
    for i in k:
        #print(ind[i])
        m = mean(data[ind[i],:])
        for j in range(len(ind[i])):
            wcssd += np.linalg.norm(data[ind[i][j], :] - m)**2
    return wcssd
    
def dist_avg(p, ps):
    n = len(ps)
    d = np.zeros(n)
    for i in range(n):
        d[i] = np.linalg.norm(p-ps[i])
    return np.mean(d)
    
def SC(data, c, k):
    ind = cut(c, k)
    S = []
    for i in range(len(data)):
        ind_in = ind[c[i]]
        ind_out = []
        for j in k:
            if j != c[i]:
                ind_out += ind[j]
        A = dist_avg(data[i,:], data[ind_in, :])
        B = dist_avg(data[i,:], data[ind_out, :])
        S.append((B-A)/np.max([A,B]))
    return np.mean(S)

def main():
    np.random.seed(0)
    data = pd.read_csv('digits-embedding.csv').to_numpy()
    cls = classify(data)
    ind = []
    for i in range(10):
        #print(list(np.random.randint(0, len(cls[i]), size=10)))
        ind = ind + [cls[i][j] for j in list(np.random.choice(len(cls[i]), size=10, replace=False))]
        #ind = ind + cls[i][np.random.randint(0, len(cls[i]), size=10)]
    subdata = data[ind, :]
    classes = data[ind, 1].astype('int')
    
    feature = subdata[:, 2:]
    Z = linkage(feature, method='single')
    #print(Z)
    plt.figure()
    dendrogram(Z)
    plt.savefig('single.png')
    plt.close()
    cutree = cluster.hierarchy.cut_tree(Z, n_clusters=[2,4,8,16,32])
    wcssd = []
    sc = []
    K = [2,4,8,16,32]
    for i in range(len(K)):
        wcssd.append(WCSSD(feature, cutree[:, i], range(K[i])))
        sc.append(SC(feature, cutree[:, i], range(K[i])))
    plt.figure()
    #print(wcssd)
    plt.plot(K, wcssd, label='WCSSD')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('wc_single.png')
    plt.close()
    plt.figure()
    plt.plot(K, sc, label='SC')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('sc_single.png')
    plt.close()
    nmi = NMI(classes, cutree[:, 3], range(10), range(16))
    print('NMI single linkage(K = 16):', nmi)
    
    Z = linkage(feature, method='complete')
    plt.figure()
    dendrogram(Z)
    plt.savefig('complete.png')
    plt.close()
    cutree = cluster.hierarchy.cut_tree(Z, n_clusters=[2,4,8,16,32])
    wcssd = []
    sc = []
    K = [2,4,8,16,32]
    for i in range(len(K)):
        wcssd.append(WCSSD(feature, cutree[:, i], range(K[i])))
        sc.append(SC(feature, cutree[:, i], range(K[i])))
    plt.figure()
    plt.plot(K, wcssd, label='WCSSD')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('wc_complete.png')
    plt.close()
    plt.figure()
    plt.plot(K, sc, label='SC')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('sc_complete.png')
    plt.close()
    nmi = NMI(classes, cutree[:, 2], range(10), range(8))
    print('NMI complete linkage(K = 8):', nmi)
    
    Z = linkage(feature, method='average')
    plt.figure()
    dendrogram(Z)
    plt.savefig('average.png')
    plt.close()
    cutree = cluster.hierarchy.cut_tree(Z, n_clusters=[2,4,8,16,32])
    wcssd = []
    sc = []
    K = [2,4,8,16,32]
    for i in range(len(K)):
        wcssd.append(WCSSD(feature, cutree[:, i], range(K[i])))
        sc.append(SC(feature, cutree[:, i], range(K[i])))
    plt.figure()
    plt.plot(K, wcssd, label='WCSSD')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('wc_average.png')
    plt.close()
    plt.figure()
    plt.plot(K, sc, label='SC')
    plt.xlabel('K')
    plt.legend(loc='upper right')
    plt.savefig('sc_average.png')
    plt.close()
    nmi = NMI(classes, cutree[:, 2], range(10), range(8))
    print('NMI average linkage(K = 8):', nmi)
    
if __name__ == '__main__':
    main()
