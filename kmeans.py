import pandas as pd
import numpy as np
import copy
import sys
import matplotlib.pyplot as plt

class KMean:
   def __init__(self, data, K, init_centroids, max_iters, labels, set_label):
       self.data = data
       self.K = K
       self.centroids = init_centroids
       self.max_iters = max_iters
       self.labels = labels
       self.set_label = set_label
       self.num_label = len(set_label)
       assert self.K == len(self.centroids)
       
   def l2norm(self, p1, p2):
       l2 = 0.0
       for i in range(len(p1)):
           l2 += (p1[i]-p2[i])**2
       l2 = np.sqrt(l2)
       return l2
       
   def assign(self, point):
       l2 = np.zeros(self.K)
       for i in range(self.K):
           l2[i] = self.l2norm(self.centroids[i], point)
       return np.argmin(l2)
       
   def meanvec(self, vecs):
       n = len(vecs)
       mean = vecs[0]
       for i in range(1,n):
           mean += vecs[i]
       mean /= n
       return mean
       
   def fit(self):
       #m = []
       for it in range(self.max_iters):
           m = []
           ind = dict()
           for i in range(self.K):
               ind[i] = []
           for i in range(len(self.data)):
               m.append(self.assign(self.data[i]))
               ind[m[-1]].append(i)
           if it < self.max_iters-1:
               for i in range(self.K):
                   self.centroids[i] = self.meanvec(self.data[ind[i], :])
       self.m = m
       self.ind = ind
       self.evaluate()
   
   def dist_avg(self, p, ps):
       n = len(ps)
       d = np.zeros(n)
       for i in range(n):
           d[i] = self.l2norm(p, ps[i])
       return np.mean(d)
       
   def SilhoutteCoef(self, i):
       m = self.m[i]
       ind_in = self.ind[m]
       ind_out = []
       for i in range(self.K):
           if i != m:
               ind_out = ind_out + self.ind[i]
       A = self.dist_avg(self.data[i], self.data[ind_in][:])
       B = self.dist_avg(self.data[i], self.data[ind_out][:])
       S = (B-A)/np.max([A,B])
       return S
       
   def ind_label(self):
       ind = dict()
       for i in range(self.num_label):
           ind[self.set_label[i]] = []
       for i in range(len(self.data)):
           ind[self.labels[i]].append(i)
       self.ind_lb = ind
   
   def entropy(self, ind):
       keys = list(ind.keys())
       p = dict()
       n = 0.0
       ep = 0.0
       for key in keys:
           n += len(ind[key])
       for key in keys:
           p[key] = len(ind[key])*1.0/n
           ep += p[key]*np.log(p[key])
       return -ep, p
       
   def matrix_p(self):
       p = np.zeros([self.num_label, self.K])
       n = len(self.data)
       for i in range(self.num_label):
           for j in range(self.K):
               its = list(set(self.ind_lb[self.set_label[i]]).intersection(set(self.ind[j])))
               p[i, j] = len(its)*1.0/n
       self.matrix_p = p
       
   def I(self):
       r = 0.0
       for i in range(self.num_label):
           for j in range(self.K):
               if self.matrix_p[i,j] > 0:
                   r += self.matrix_p[i,j]*np.log(self.matrix_p[i,j]/self.pc[self.set_label[i]]/self.pg[j])
       return r 
       
   def evaluate(self):
       self.wc_ssd = 0.0
       self.sc = 0.0
       n = len(self.data)
       for i in range(n):
           self.wc_ssd += self.l2norm(self.centroids[self.m[i]], self.data[i])**2
           self.sc += self.SilhoutteCoef(i)/n
       self.ind_label()
       hc, self.pc = self.entropy(self.ind_lb)
       hg, self.pg = self.entropy(self.ind)
       self.matrix_p()
       r = self.I()
       self.nmi = r/(hc+hg)
       
def main():
    np.random.seed(0)
    data = pd.read_csv(sys.argv[1]).to_numpy()
    #data = data[0:1000,:]
    K = int(sys.argv[2])
    max_iters = 500
    set_label = [0,1,2,3,4,5,6,7,8,9]
    labels = data[:, 1].astype('int')
    ind = np.random.choice(len(data), size=K, replace=False)
    init_centroids = data[ind, 2:]
    data = data[:, 2:]
    KM = KMean(data, K, init_centroids, max_iters, labels, set_label)
    KM.fit()
    #print(KM.pc, KM.pg, KM.matrix_p)
    print('WC-SSD:', KM.wc_ssd)
    print('SC:', KM.sc)
    print('NMI:', KM.nmi)
    
if __name__ == '__main__':
    main()
