import pandas as pd
import numpy as np
import copy
import sys
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from kmeans import KMean

def cut(data, lbs):
    n = len(data)
    ind = []
    for i in range(n):
        if int(data[i, 1]) in lbs:
            ind.append(i)
    return data[ind, :]
    
def analysis(verbose):
    #data1 = pd.read_csv('digits-embedding.csv')
    #data1 = data1.sample(frac=0.1).to_numpy()
    data1 = pd.read_csv('digits-embedding.csv').sample(frac=0.1).to_numpy()
    data2 = cut(data1, [2,4,6,7])
    data3 = cut(data1, [6,7])
    
    K = [2,4,8,16,32]
    #K = [4,8]
    max_iters = 50

     
    wc = []
    sc = []
    for k in K:
        set_label = [0,1,2,3,4,5,6,7,8,9]
        labels = data1[:, 1].astype('int')
        ind = np.random.choice(len(labels), size=k, replace=False)
        data = data1[:, 2:]
        init_centroids = data[ind, :]
        KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
        KM.fit()
        wc.append(KM.wc_ssd)
        sc.append(KM.sc)
    if verbose:
        print('WC SSD Dataset1:', wc)
        print('SC Dataset1:', sc)
        plt.figure()
        plt.plot(K, wc, label='WC SSD')
        plt.savefig('wc1.png')
        plt.figure()
        plt.plot(K, sc, label='SC')
        plt.savefig('sc1.png')
    wc1 = wc
    sc1 = sc
    
    #print('???')
    wc = []
    sc = []
    for k in K:
        set_label = [2,4,6,7]
        labels = data2[:, 1].astype('int')
        ind = np.random.choice(len(labels), size=k, replace=False)
        data = data2[:, 2:]
        init_centroids = data[ind, :]
        #print(k)
        KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
        KM.fit()
        wc.append(KM.wc_ssd)
        sc.append(KM.sc)
    if verbose:
        print('WC SSD Dataset2:', wc)
        print('SC Dataset2:', sc)
        plt.figure()
        plt.plot(K, wc, label='WC SSD')
        plt.savefig('wc2.png')
        plt.figure()
        plt.plot(K, sc, label='SC')
        plt.savefig('sc2.png')
    wc2 = wc
    sc2 = sc
    
    wc = []
    sc = []
    for k in K:
        set_label = [6,7]
        labels = data3[:, 1].astype('int')
        ind = np.random.choice(len(labels), size=k, replace=False)
        data = data3[:, 2:]
        init_centroids = data[ind, :]
        KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
        KM.fit()
        wc.append(KM.wc_ssd)
        sc.append(KM.sc)
    if verbose:
        print('WC SSD Dataset3:', wc)
        print('SC Dataset3:', sc)
        plt.figure()
        plt.plot(K, wc, label='WC SSD')
        plt.savefig('wc3.png')
        plt.figure()
        plt.plot(K, sc, label='SC')
        plt.savefig('sc3.png')
    wc3 = wc
    sc3 = sc
    
    return wc1, sc1, wc2, sc2, wc3, sc3
    
def stat(M):
    mean = np.mean(M, axis=0)
    stderr = np.zeros(len(M[0]))
    for i in range(len(M[0])):
        for j in range(len(M)):
            #print(M[i][j])
            stderr[i] += np.square(M[j][i]-mean[i])/len(M)
    stderr = np.sqrt(stderr)
    return mean, stderr
    
    
def main():
    '''
    np.random.seed(0)
    analysis(1)
    '''
    #M = [[3,4,5,6,7], [4,2,5,6,7], [1,23,5,7,4], [3,5,2,5,6], [4,2,6,8,9]]

    #mean, stderr = stat(M)

    times = 2
    WC1 = [[0, 0, 0, 0, 0]]
    #WC1 = [[0,0]]
    #WC1 = [[]]
    SC1 = [[0, 0, 0, 0, 0]]
    WC2 = [[0, 0, 0, 0, 0]]
    SC2 = [[0, 0, 0, 0, 0]]
    WC3 = [[0, 0, 0, 0, 0]]
    SC3 = [[0, 0, 0, 0, 0]]
    K = [2,4,8,16,32]
    for i in range(times):
        np.random.seed(i)
        wc1, sc1, wc2, sc2, wc3, sc3 = analysis(0)
        WC1.append(wc1)
        SC1.append(sc1)
        WC2.append(wc2)
        SC2.append(sc2)
        WC3.append(wc3)
        SC3.append(sc3)
        #print(wc1, sc1, wc2, sc2, wc3, sc3)
    #print(WC1)
    del WC1[0]
    del WC2[0]
    del WC3[0]
    del SC1[0]
    del SC2[0]
    del SC3[0]
    #print(WC1, SC1)
    mean_wc1, stderr_wc1 = stat(WC1)
    mean_sc1, stderr_sc1 = stat(SC1)
    mean_wc2, stderr_wc2 = stat(WC2)
    mean_sc2, stderr_sc2 = stat(SC2)
    mean_wc3, stderr_wc3 = stat(WC3)
    mean_sc3, stderr_sc3 = stat(SC3)
    plt.figure()
    plt.errorbar(K, mean_wc1, yerr=stderr_wc1, label='WC SSD')
    plt.savefig('als1_wc.png')
    plt.figure()
    plt.errorbar(K, mean_sc1, yerr=stderr_sc1, label='SC')
    plt.savefig('als1_sc.png')
    plt.figure()
    plt.errorbar(K, mean_wc2, yerr=stderr_wc2, label='WC SSD')
    plt.savefig('als2_wc.png')
    plt.figure()
    plt.errorbar(K, mean_sc2, yerr=stderr_sc2, label='SC')
    plt.savefig('als2_sc.png')
    plt.figure()
    plt.errorbar(K, mean_wc3, yerr=stderr_wc3, label='WC SSD')
    plt.savefig('als3_wc.png')
    plt.figure()
    plt.errorbar(K, mean_sc3, yerr=stderr_sc3, label='SC')
    plt.savefig('als3_sc.png')
    
    print('***********************************')
    max_iters = 50

    data1 = pd.read_csv('digits-embedding.csv').to_numpy()
    #data1 = pd.read_csv('digits-embedding.csv').sample(frac=0.1).to_numpy()
    data2 = cut(data1, [2,4,6,7])
    data3 = cut(data1, [6,7])
    
    k = 4
    set_label = [0,1,2,3,4,5,6,7,8,9]
    labels = data1[:, 1].astype('int')
    ind = np.random.choice(len(labels), size=k, replace=False)
    data = data1[:, 2:]
    init_centroids = data[ind, :]
    KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
    KM.fit()
    print('NMI Dataset1:', KM.nmi)
    size = 1000
    indices = np.random.choice(len(data1), size=size, replace=False)
    x = np.zeros(size)
    y = np.zeros(size)
    c = np.zeros(size)
    for i in range(size):
        #print(indices[i])
        x[i] = data1[indices[i]][2]
        y[i] = data1[indices[i]][3]
        c[i] = KM.m[indices[i]]
    plt.scatter(x, y, c=c)
    plt.savefig('cluster1_' + '%2d' % k + '.png')
    plt.show()
    
    k = 4
    set_label = [2,4,6,7]
    labels = data2[:, 1].astype('int')
    ind = np.random.choice(len(labels), size=k, replace=False)
    data = data2[:, 2:]
    init_centroids = data[ind, :]
    KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
    KM.fit()
    print('NMI Dataset2:', KM.nmi)
    size = 1000
    print(len(data2))
    indices = np.random.choice(len(data2), size=size, replace=False)
    x = np.zeros(size)
    y = np.zeros(size)
    c = np.zeros(size)
    for i in range(size):
        #print(indices[i])
        x[i] = data2[indices[i]][2]
        y[i] = data2[indices[i]][3]
        c[i] = KM.m[indices[i]]
    plt.scatter(x, y, c=c)
    plt.savefig('cluster2_' + '%2d' % k + '.png')
    plt.show()
    
    k = 4
    set_label = [6,7]
    labels = data3[:, 1].astype('int')
    ind = np.random.choice(len(labels), size=k, replace=False)
    data = data3[:, 2:]
    init_centroids = data[ind, :]
    KM = KMean(data, k, init_centroids, max_iters, labels, set_label)
    KM.fit()
    print('NMI Dataset3:', KM.nmi)
    size = 1000
    print(len(data3))
    indices = np.random.choice(len(data3), size=size, replace=False)
    x = np.zeros(size)
    y = np.zeros(size)
    c = np.zeros(size)
    for i in range(size):
        #print(indices[i])
        x[i] = data3[indices[i]][2]
        y[i] = data3[indices[i]][3]
        c[i] = KM.m[indices[i]]
    plt.scatter(x, y, c=c)
    plt.savefig('cluster3_' + '%2d' % k  + '.png')
    plt.show()
    
    
if __name__ == '__main__':
    main()
